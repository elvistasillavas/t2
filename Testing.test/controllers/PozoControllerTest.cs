﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using validator.ValidatorEntities;
using Model.Entidades;
using System.Web.Mvc;
using NUnit.Framework;
using MvcApplication2.Controllers;
using Interface.interfaces;
using Moq;

namespace Testing.test.controllers
{
    [TestFixture]

    public class PozoControllerTest
    {
      

        [Test]
        public void TestPostCreatePosoOKReturnRedirect()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePzo>();

            repositoryMock.Setup(o => o.Store(new Pozo()));

            var validatorMock = new Mock<validatroPozo>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            var controller = new PozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(pozo);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreatePosoCallStoreMethodFromRepository()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePzo>();

            var validatorMock = new Mock<validatroPozo>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            repositoryMock.Setup(o => o.Store(pozo));

            var controller = new PozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Create(pozo);

            repositoryMock.Verify(o => o.Store(pozo), Times.Once());

        }

        [Test]

        public void TestPostCreateReturnPosoViewWithErrorsWhenValidationFail()
        {
            var pozo = new Pozo { };

            var mock = new Mock<validatroPozo>();

            mock.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new PozoController(null, mock.Object);

            var view = controller.Create(pozo);

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

        [Test]
        public void TestAsignarPersonaAPozo()
        {
            var pozo = new Pozo { };

            var mock = new Mock<InterfacePzo>();
            mock.Setup(o => o.All()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());
            mock.Setup(o => o.AsignarPersonaPozo(pozo, "Juan")).Returns(new Pozo());
            var mockv = new Mock<validatroPozo>();

            mockv.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new PozoController(mock.Object, mockv.Object);

            var view = controller.AsignarPozoAPersona();

            Assert.IsInstanceOf(typeof(ViewResult), view);




        }
    }
}
