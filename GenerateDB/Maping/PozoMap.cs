﻿using Model.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateDB.Maping
{
    public class PozoMap: EntityTypeConfiguration<Pozo>
    {
         public PozoMap()
        {
        this.ToTable("pozo");

           
            this.Property(o => o.Nombre).HasColumnName("nombre").HasMaxLength(250); ;

            this.Property(o => o.Propietario).HasColumnName("propietario").HasMaxLength(250);

            this.Property(o => o.Ubicacion).HasColumnName("ubicacion").HasMaxLength(250);
          
             this.Property(o => o.Nombre).HasColumnName("Id");

            this.Property(o => o.Propietario).HasColumnName("Code").HasMaxLength(250);

            this.Property(o => o.Ubicacion).HasColumnName("Name").HasMaxLength(250);
       
         }
         }
}
