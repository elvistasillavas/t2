﻿using Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Interface.interfaces;
using validator.ValidatorEntities;

namespace MvcApplication2.Controllers
{
    public class PozoController : Controller
    {
        private InterfacePzo repository;
        private validatroPozo  validator;

        public PozoController(InterfacePzo repository, validatroPozo validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        //
        public ViewResult Index()
        {
            return View("Inicio");
        }
        [HttpGet]
        public ViewResult Create()
        {
            return View("Create");
        }



        [HttpPost]
        public ActionResult Create(Pozo pozo)
        {

            if (validator.Pass(pozo))
            {
                repository.Store(pozo);

                TempData["UpdateSuccess"] = "Se envio el Correo Satisfactoriamente";

                return RedirectToAction("Index");
            }

            return View("Inicio", pozo);
        }

        [HttpPost]
        public ActionResult AsignarPozoAPersona()
        {

            return View("Inicio");

        }

    }
}
