﻿using Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.interfaces
{
   public interface InterfacePzo
    {
        List<Pozo> All();

        void Store(Pozo pozo);
        List<Pozo> ByQueryAll(string query);
        Pozo AsignarPersonaPozo(Pozo pozo, string Personanombre);
    }
}
