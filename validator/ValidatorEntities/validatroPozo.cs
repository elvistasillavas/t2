﻿using Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace validator.ValidatorEntities
{
   public  class validatroPozo
    {
       public virtual bool Pass(Pozo pozo)
       {
           if (String.IsNullOrEmpty(pozo.Nombre))
               return false;
           if (String.IsNullOrEmpty(pozo.Propietario))
               return false;
           if (String.IsNullOrEmpty(pozo.CordenadaEste))
               return false;
           if (String.IsNullOrEmpty(pozo.CordenadaNore))
               return false;
           return true;
       }
    }
}
